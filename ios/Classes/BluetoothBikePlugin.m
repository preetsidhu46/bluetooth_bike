#import "BluetoothBikePlugin.h"
#import <bluetooth_bike/bluetooth_bike-Swift.h>

@implementation BluetoothBikePlugin
+ (void)registerWithRegistrar:(NSObject<FlutterPluginRegistrar>*)registrar {
  [SwiftBluetoothBikePlugin registerWithRegistrar:registrar];
}
@end
