class BluetoothState {
  static const BLUETOOTH_TURNED_ON = 'bluetoothTurnedOn';
  static const BLUETOOTH_TURNED_OFF = 'bluetoothTurnedOff';
  static const CONNECTION_CLOSED = 'conectionClosed';
  static const BIKE_LOCK = 'bikeLock';
  static const ERROR_SERVICE = 'errorService';
  static const BIKE_UNLOCK = 'bikeUnlock';
  static const BIKE_CONNECTED = 'stateConnected';
  static const BIKE_DISCONNECTED = 'stateDisconnected';
  static const DISCONNECTING = 'stateDisconnecting';
  static const STATE_CONNECTING = 'stateConnecting';
  static const AUTHENTICATED = 'authenticated';
  static const AUTHENTICATION_FAILED = 'authenticationFailed';
  static const HANDLE_LOCK = 'handleLock';
  static const HANDLE_UNLOCK = 'handleUnLock';
  static const IGNITION_ON = 'ignitionOn';
  static const ERROR = 'error';
  static const TOKEN_KEY_NULL = 'tokenKeyNull';
  static const ENCRYPTION_KEY_NULL = 'encryptionKeyNull';
}
