import 'package:bluetooth_bike/strings.dart';
import 'package:flutter/services.dart';

class BluetoothBike {
  static final MethodChannel channel = MethodChannel('com.hawkeye.bikeservice');

  bool isConnected = false;
  KnobStatus currentKnobStatus = KnobStatus.HANDLE_LOCKED;

  // static const BikeType defaultBikeType = BikeType.INVERS;
  Map bikeData;

  Function callbackFunction;
  BluetoothBike() {
    channel.setMethodCallHandler((m) => setupMethodHandler(m));
  }
  addCallback({Function callback}) {
    callbackFunction = callback;
  }

  setupMethodHandler(m) async {
    print(m);
    BikeType bikeType;
    if (m.arguments != null) {
      Map data = m.arguments;
      bikeType =
          data["bikeType"] == "invers" ? BikeType.INVERS : BikeType.BEAST;
    } else {
      bikeType = BikeType.INVERS;
    }
    String channelName = m.method.trim();
    print('channel name: $channelName');
    switch (channelName) {
      case BluetoothState.BLUETOOTH_TURNED_ON:
        callbackFunction(methodName: channelName, bikeType: bikeType);
        break;
      case BluetoothState.BLUETOOTH_TURNED_OFF:
        callbackFunction(methodName: channelName, bikeType: bikeType);
        break;
      case BluetoothState.CONNECTION_CLOSED:
        callbackFunction(methodName: channelName, bikeType: bikeType);
        break;
      case BluetoothState.BIKE_LOCK:
        if (!isConnected && bikeType == BikeType.BEAST) return;
        callbackFunction(methodName: channelName, bikeType: bikeType);
        break;
      case BluetoothState.ERROR_SERVICE:
        callbackFunction(methodName: channelName, bikeType: bikeType);
        break;
      case BluetoothState.BIKE_UNLOCK:
        if (!isConnected && bikeType == BikeType.BEAST) return;
        callbackFunction(methodName: channelName, bikeType: bikeType);
        break;
      case BluetoothState.STATE_CONNECTING:
        callbackFunction(methodName: channelName, bikeType: bikeType);
        break;
      case BluetoothState.BIKE_CONNECTED:
        callbackFunction(methodName: channelName, bikeType: bikeType);
        break;

      case BluetoothState.BIKE_DISCONNECTED:
        if (bikeType == BikeType.BEAST) isConnected = false;
        callbackFunction(methodName: channelName, bikeType: bikeType);
        print('stateDisconnected');
        break;
      case BluetoothState.DISCONNECTING:
        print('BTchannel stateDisconnecting');
        callbackFunction(methodName: channelName, bikeType: bikeType);
        break;
      case BluetoothState.AUTHENTICATED:
        //actual connection happend. After authentication we can show the unlock option
        print('BTchannel AUTHENTICATED');
        isConnected = true;
        callbackFunction(methodName: channelName, bikeType: bikeType);
        break;
      case BluetoothState.AUTHENTICATION_FAILED:
        isConnected = false;
        print('BTchannel AUTHENTICATION_FAILED');
        callbackFunction(methodName: channelName, bikeType: bikeType);
        break;
      case BluetoothState.HANDLE_LOCK:
        print('BTchannel HANDLE_LOCK isConnected : $isConnected');
        if (!isConnected) return;
        currentKnobStatus = KnobStatus.HANDLE_LOCKED;
        callbackFunction(methodName: channelName, bikeType: bikeType);
        break;
      case BluetoothState.HANDLE_UNLOCK:
        print('BTchannel HANDLE_UNLOCK isConnected : $isConnected');
        if (!isConnected) return;
        currentKnobStatus = KnobStatus.HANDLE_UNLOCKED;
        callbackFunction(methodName: channelName, bikeType: bikeType);
        break;
      case BluetoothState.IGNITION_ON:
        print('BTchannel IGNITION_ON isConnected : $isConnected');
        currentKnobStatus = KnobStatus.IGNITION_ON;
        if (!isConnected) return;
        callbackFunction(methodName: channelName, bikeType: bikeType);
        break;
      case BluetoothState.ERROR:
        print('BTchannel Knob ERROR');
        break;
      default:
        print('BTchannel no method');
    }
  }

  addData({Map data, BikeType bikeType}) {
    bikeData = data;
    print("$bikeType data $bikeData ");
  }

  checkDataAvailable() {
    return bikeData == null;
  }

  checkBLEConnection({BikeType bikeType}) async {
    bool check = await BluetoothBike.channel
        .invokeMethod('checkBleConnection', getArgument(bikeType));
    print('BTchannel check: $check');
    return check;
  }

  checkBlePermission({BikeType bikeType}) async {
    String perm = await BluetoothBike.channel
        .invokeMethod('bluetoothPerm', getArgument(bikeType));
    print('prem==>>> $perm');
  }

  unRegister({BikeType bikeType}) async {
    bikeData = null;
    await BluetoothBike.channel.invokeMethod('unRegister');
  }

  setDataToService({BikeType bikeType}) async {
    print("setDataToService $bikeData");
    if (bikeData != null && bikeData.isNotEmpty) {
      if (!bikeData.containsKey('tokenKey') ||
          !isValidTokenKey(bikeData['tokenKey'])) {
        print("TokenKey =====>>>  ${bikeData['tokenKey']}");
        callbackFunction(methodName: "tokenKeyNull", bikeType: bikeType);
        return;
      }
      if (bikeType == BikeType.BEAST) {
        if (!bikeData.containsKey("encryptionKey") ||
            bikeData['encryptionKey'] == '') {
          print("encryptionKey =====>>>  ${bikeData['encryptionKey']}");
          callbackFunction(methodName: "encryptionKeyNull", bikeType: bikeType);
          return;
        }
      }
      bikeData.addAll(getArgument(bikeType));
      print('=================');
      print('ride service data');
      print(bikeData);
      print('=================');
      String status =
          await BluetoothBike.channel.invokeMethod('bluetoothData', bikeData);
      print('status->>>>>> $status');
      await performActionToBike(
          bikeAction: BikeAction.BLUETOOTH_TURNED_ON, bikeType: bikeType);
    }
  }

  isValidTokenKey(String key) {
    if (key == '' || key.length <= 6 || key == '&&&&&&') {
      return false;
    } else {
      return true;
    }
  }

  performActionToBike({BikeAction bikeAction, BikeType bikeType}) async {
    String action = getBikeAction(bikeAction, bikeType);
    Map rideServiceData = {
      'bikeState': action.toString(),
    }..addAll(getArgument(bikeType));
    print('in c===>> $rideServiceData');
    String here =
        await BluetoothBike.channel.invokeMethod('bleConnect', rideServiceData);
    print('cble => $here');
  }

  // bikeIsConnected({BikeType bikeType = defaultBikeType}) {
  //   return bikeType == BikeType.INVERS
  //       ? _inversService.bikeIsConnected()
  //       : _beastService.bikeIsConnected();
  // }
  bool bikeIsConnected() {
    return isConnected;
  }

  getBikeLastStatus() async {
    Map bikeStatus = await BluetoothBike.channel
        .invokeMethod('getLastStatus', getArgument(BikeType.BEAST));
    print('getLastBikeStatus => $bikeStatus');
    return bikeStatus;
  }

  bleOff() async {
    await BluetoothBike.channel.invokeMethod('bleOff');
  }

  getBikeAction(BikeAction bikeAction, BikeType bikeType) {
    switch (bikeAction) {
      case BikeAction.CONNECT:
        return "connect";
      case BikeAction.CLOSE:
        return "close";
      case BikeAction.UNLOCK:
        return "unLock";
      case BikeAction.LOCK:
        return "lock";
      case BikeAction.TRUNK_OPEN:
        return "trunkOpen";
      case BikeAction.BLUETOOTH_TURNED_ON:
        return "turnedOn";
      case BikeAction.BLUETOOTH_DENIED:
        return "bleDenied";
      case BikeAction.FETCH_BIKE_STATUS:
        return "fetchBikeData";
      case BikeAction.LOCATE_BIKE:
        return "locateBike";
      case BikeAction.FETCH_ODOMETER_DATA:
        return "fetchOdometerData";
      default:
        return "";
    }
  }

  getBikeActionType(String bikeAction, {BikeType bikeType}) {
    switch (bikeAction) {
      case "connect":
        return BikeAction.CONNECT;
      case "close":
        return BikeAction.CLOSE;
      case "unLock":
        return BikeAction.UNLOCK;
      case "lock":
        return BikeAction.LOCK;
      case "trunkOpen":
        return BikeAction.TRUNK_OPEN;
      case "unlockTrunkOpen":
        return BikeAction.UNLOCK_TRUNK_OPEN;
      case "turnedOn":
        return BikeAction.BLUETOOTH_TURNED_ON;
      case "bleDenied":
        return BikeAction.BLUETOOTH_DENIED;
      case "fetchBikeData":
        return BikeAction.FETCH_BIKE_STATUS;
      case "locateBike":
        return BikeAction.LOCATE_BIKE;
      case "fetchOdometerData":
        return BikeAction.FETCH_ODOMETER_DATA;

      default:
        return BikeAction.NO_ACTION;
    }
  }

  getArgument(BikeType bikeType) {
    return {
      "bikeType": bikeType == BikeType.INVERS ? "invers" : "beast",
    };
  }
}

enum BikeAction {
  CLOSE,
  UNLOCK,
  LOCK,
  TRUNK_OPEN,
  UNLOCK_TRUNK_OPEN,
  BLUETOOTH_TURNED_ON,
  BLUETOOTH_DENIED,
  FETCH_BIKE_STATUS,
  LOCATE_BIKE,
  CONNECT,
  FETCH_ODOMETER_DATA,
  NO_ACTION
}
enum BikeType {
  INVERS,
  BEAST,
}

enum KnobStatus {
  HANDLE_LOCKED,
  HANDLE_UNLOCKED,
  IGNITION_ON,
}
