package com.bounceshare.bluetooth_bike.util;

public class MethodCalls {

    // App white list method call
    final public static String APP_WHITE_LIST_METHOD = "navigateToAutoStart";

    public interface BluetoothState {
        // Bluetooth helmet method calls
        String BLE_OFF = "bleOff";
        String BLUETOOTH_TURNED_OFF = "bluetoothTurnedOff";
        String BLUETOOTH_TURNED_ON = "bluetoothTurnedOn";
        String BLUETOOTH_PERMISSION = "bluetoothPerm";
        String CHECK_BLUETOOTH_CONNECTION = "checkBleConnection";
    }

    public interface BluetoothBike {
        // Bluetooth helmet method calls
        String BIKE_CONNECTION_ESTABLISH = "bluetoothData";
        String BIKE_CONNECTION_STATE_CHANGE = "bleConnect";
        String BIKE_LOCK = "bikeLock";
        String BIKE_UNLOCK = "bikeUnlock";
        String BIKE_ERROR = "errorService";
        String BIKE_CONNECTED = "stateConnected";
        String BIKE_CONNECTING = "stateConnecting";
        String BIKE_DISCONNECTED = "stateDisconnected";
        String BIKE_DISCONNECTING = "stateDisconnecting";
        String BIKE_CONNECTION_CLOSED = "connectionClosed";
        String UNREGISTER_SERVICE = "unRegister";
        String AUTHENTICATED = "authenticated";
        String AUTHENTICATION_FAILED = "authenticationFailed";
        String GET_LAST_STATUS = "getLastStatus";

        interface ConnectionAction {
            String BLUETOOTH_TURNED_ON = "turnedOn";
            String CONNECT = "connect";
            String CLOSE = "close";
            String FETCH_BIKE_STATUS = "fetchBikeData";
            String FETCH_ODOMETER_DATA = "fetchOdometerData";
            String LOCATE_BIKE = "locateBike";
            String LOCK = "lock";
            String TRUNK_OPEN = "trunkOpen";
            String UNLOCK_TRUNK_OPEN = "UnlockTrunkOpen";
            String UNLOCK = "unLock";
            String BLUETOOTH_DENIED = "bleDenied";

        }

        interface KnobStatus {
            String HANDLE_LOCK = "handleLock";
            String HANDLE_UNLOCK = "handleUnLock";
            String IGNITION_ON = "ignitionOn";
            String ERROR = "error";

        }
    }

}
