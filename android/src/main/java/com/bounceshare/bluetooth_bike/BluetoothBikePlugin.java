package com.bounceshare.bluetooth_bike;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Lifecycle;
import androidx.lifecycle.LifecycleOwner;
import androidx.lifecycle.Observer;

import com.bounceshare.bluetooth_bike.bluetooth.BluetoothStateUtil;
import com.bounceshare.bluetooth_bike.bluetooth.IBluetoothStateChange;
import com.bounceshare.bluetooth_bike.util.MethodCalls;
import com.bounceshare.bluetooth_bike.util.MethodChannels;

import io.flutter.plugin.common.MethodCall;
import io.flutter.plugin.common.MethodChannel;
import io.flutter.plugin.common.MethodChannel.MethodCallHandler;
import io.flutter.plugin.common.MethodChannel.Result;
import io.flutter.plugin.common.PluginRegistry;
import io.flutter.plugin.common.PluginRegistry.Registrar;

import com.bounceshare.bluetooth_library.invers.BikeStatus;
import com.bounceshare.bluetooth_library.invers.InversRideService;
import com.bounceshare.bluetooth_library.service.RideServiceDetails;
import com.bounceshare.bluetooth_library.beast.BeastBikeStatus;
import com.bounceshare.bluetooth_library.beast.BeastRideService;
import java.util.HashMap;
import java.util.Map;

/**
 * BluetoothBikePlugin
 */
public class BluetoothBikePlugin extends AppCompatActivity implements MethodCallHandler, LifecycleOwner {
    private static MethodChannel methodChannel;
    private static Activity activity;
    private static LifecycleOwner lifecycleOwner;

    /**
     * Plugin registration.
     */
    public static void registerWith(Registrar registrar) {
        methodChannel = new MethodChannel(registrar.messenger(), MethodChannels.BLUETOOTH_BIKE_CHANNEL);
        methodChannel.setMethodCallHandler(new BluetoothBikePlugin());
        activity = registrar.activity();
        lifecycleOwner = (LifecycleOwner) registrar.activity();
        Log.e("lifecycleOwner", "" + lifecycleOwner);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    private InversRideService.NotificationAction getConnectionDataInvers(String connection) {
        switch (connection) {
            case MethodCalls.BluetoothBike.ConnectionAction.BLUETOOTH_TURNED_ON:
                return InversRideService.NotificationAction.BLUETOOTH_TURNED_ON;
            case MethodCalls.BluetoothBike.ConnectionAction.CONNECT:
                return InversRideService.NotificationAction.CONNECT;
            case MethodCalls.BluetoothBike.ConnectionAction.CLOSE:
                return InversRideService.NotificationAction.CLOSE;
            case MethodCalls.BluetoothBike.ConnectionAction.FETCH_BIKE_STATUS:
                return InversRideService.NotificationAction.FETCH_BIKE_STATUS;
            case MethodCalls.BluetoothBike.ConnectionAction.FETCH_ODOMETER_DATA:
                return InversRideService.NotificationAction.FETCH_ODOMETER_DATA;
            case MethodCalls.BluetoothBike.ConnectionAction.LOCATE_BIKE:
                return InversRideService.NotificationAction.LOCATE_BIKE;
            case MethodCalls.BluetoothBike.ConnectionAction.LOCK:
                return InversRideService.NotificationAction.LOCK;
            case MethodCalls.BluetoothBike.ConnectionAction.TRUNK_OPEN:
                return InversRideService.NotificationAction.TRUNK_OPEN;
            case MethodCalls.BluetoothBike.ConnectionAction.UNLOCK_TRUNK_OPEN:
                return InversRideService.NotificationAction.UNLOCK_TRUNK_OPEN;
            case MethodCalls.BluetoothBike.ConnectionAction.UNLOCK:
                return InversRideService.NotificationAction.UNLOCK;
            case MethodCalls.BluetoothBike.ConnectionAction.BLUETOOTH_DENIED:
                return InversRideService.NotificationAction.BLUETOOTH_DENIED;
            default:
                return null;
        }
    }

    private BeastRideService.NotificationAction getConnectionDataBeast(String connection) {
        switch (connection) {
            case MethodCalls.BluetoothBike.ConnectionAction.BLUETOOTH_TURNED_ON:
                return BeastRideService.NotificationAction.BLUETOOTH_TURNED_ON;
            case MethodCalls.BluetoothBike.ConnectionAction.CONNECT:
                return BeastRideService.NotificationAction.CONNECT;
            case MethodCalls.BluetoothBike.ConnectionAction.CLOSE:
                return BeastRideService.NotificationAction.CLOSE;
            case MethodCalls.BluetoothBike.ConnectionAction.FETCH_BIKE_STATUS:
                return BeastRideService.NotificationAction.FETCH_BIKE_STATUS;
            case MethodCalls.BluetoothBike.ConnectionAction.FETCH_ODOMETER_DATA:
                return BeastRideService.NotificationAction.FETCH_ODOMETER_DATA;
            case MethodCalls.BluetoothBike.ConnectionAction.LOCATE_BIKE:
                return BeastRideService.NotificationAction.LOCATE_BIKE;
            case MethodCalls.BluetoothBike.ConnectionAction.LOCK:
                return BeastRideService.NotificationAction.LOCK;
            case MethodCalls.BluetoothBike.ConnectionAction.TRUNK_OPEN:
                return BeastRideService.NotificationAction.TRUNK_OPEN;
            case MethodCalls.BluetoothBike.ConnectionAction.UNLOCK:
                return BeastRideService.NotificationAction.UNLOCK;
            case MethodCalls.BluetoothBike.ConnectionAction.BLUETOOTH_DENIED:
                return BeastRideService.NotificationAction.BLUETOOTH_DENIED;
            default:
                return null;
        }
    }

    private static BikeStatus bikeBLEStatus = BikeStatus.BLANK;

    private static RideServiceDetails rideServiceDetails;

    private static IBluetoothStateChange iBluetoothStateChange = state -> {
        switch (state) {
            case BT_ON:
                methodChannel.invokeMethod(MethodCalls.BluetoothState.BLUETOOTH_TURNED_ON, null);
                break;
            case BT_OFF:
                methodChannel.invokeMethod(MethodCalls.BluetoothState.BLUETOOTH_TURNED_OFF, null);
                break;
        }
    };

    @Override
    public void onMethodCall(MethodCall methodCall, Result result) {
        String callName = methodCall.method.trim();
        BikeType bikeType;
        if (methodCall.arguments != null){
            Map data = (Map) methodCall.arguments;
            bikeType = data.get("bikeType").toString().equalsIgnoreCase("invers") ? BikeType.INVERS : BikeType.BEAST;
        } else {
            bikeType = BikeType.INVERS;
        }

        switch (callName) {
            case MethodCalls.BluetoothState.BLUETOOTH_PERMISSION:
                BluetoothStateUtil.setBluetoothStateChangeForBike(iBluetoothStateChange);
                boolean status = BluetoothStateUtil.checkBluetoothPermission(activity);
                if (status){
                    methodChannel.invokeMethod(MethodCalls.BluetoothState.BLUETOOTH_TURNED_ON,
                            getDefaultArgument(bikeType));
                }
                result.success("done");
                break;
            case MethodCalls.BluetoothState.BLE_OFF:
                BluetoothStateUtil.bleOff();
                result.success("done");
                break;
            case MethodCalls.BluetoothState.CHECK_BLUETOOTH_CONNECTION:
                result.success(BluetoothStateUtil.isBluetoothEnabled());
                break;
            case MethodCalls.BluetoothBike.BIKE_CONNECTION_ESTABLISH:
                Map rideServiceData = (Map) methodCall.arguments;
                Log.e("ride details", "" + rideServiceData);
                String tokenCode = rideServiceData.get("tokenKey").toString();
                Integer bikeId = (Integer) rideServiceData.get("bikeId");
                String bleMac = rideServiceData.get("bleMac").toString();
                String licensePlate = rideServiceData.get("licensePlate").toString();
                String encryptionKey = "";
                if (bikeType == BikeType.BEAST)
                    encryptionKey = rideServiceData.get("encryptionKey").toString();
                boolean isElectric = false;
                if (rideServiceData.containsKey("isElectric")){
                    isElectric = (boolean) rideServiceData.get("isElectric");
                }
                rideServiceDetails = new RideServiceDetails(0, licensePlate, "", "", tokenCode,
                        12.915646d, 77.598919d, "", "", "", "", 0,  0L,
                        bikeType == BikeType.INVERS ? "inverse" : "beast", bleMac,
                        bikeType == BikeType.INVERS ? tokenCode : encryptionKey, bikeId, isElectric);
                ObserveConnectedBikeStates(bikeType);

                ObserveBikeConnectionStatus(bikeType);

                if (bikeType == BikeType.BEAST)
                    ObserveKnobStatus();

                result.success("done");

                break;
            case MethodCalls.BluetoothBike.BIKE_CONNECTION_STATE_CHANGE:
                rideServiceData = (Map) methodCall.arguments;
                Log.e("data", "" + rideServiceDetails);
                String connection = rideServiceData.get("bikeState").toString();

                Intent intentC;
                Log.e("applicationContext", "" + activity.getApplicationContext());
                if (bikeType == BikeType.INVERS){
                    intentC = InversRideService.Companion.buildIntent(activity.getApplicationContext(), rideServiceDetails,
                            null, getConnectionDataInvers(connection));
                } else {
                    intentC = BeastRideService.Companion.buildIntent(activity.getApplicationContext(), rideServiceDetails,
                            null, getConnectionDataBeast(connection));
                }

                if (connection.equals("close")){
                    methodChannel.invokeMethod(MethodCalls.BluetoothBike.BIKE_CONNECTION_CLOSED,
                            getDefaultArgument(bikeType));
                }
                activity.startService(intentC);
                result.success("done");
                break;
            case MethodCalls.BluetoothBike.UNREGISTER_SERVICE:
                try {
                    if (bikeType == BikeType.INVERS){
                        Intent intentLock = InversRideService.Companion.buildIntent(activity.getApplicationContext(),
                                rideServiceDetails, null, InversRideService.NotificationAction.LOCK);
                        activity.startService(intentLock);
                        Intent intentClose = InversRideService.Companion.buildIntent(activity.getApplicationContext(),
                                rideServiceDetails, null, InversRideService.NotificationAction.CLOSE);
                        activity.startService(intentClose);
                    } else {
                        Intent intentLock = BeastRideService.Companion.buildIntent(activity.getApplicationContext(),
                                rideServiceDetails, null, BeastRideService.NotificationAction.LOCK);
                        activity.startService(intentLock);
                        Intent intentClose = BeastRideService.Companion.buildIntent(activity.getApplicationContext(),
                                rideServiceDetails, null, BeastRideService.NotificationAction.CLOSE);
                        activity.startService(intentClose);
                    }
                } catch (Exception e) {
                    Log.e("error", e.toString());
                }
                try {
                    methodChannel.invokeMethod(MethodCalls.BluetoothBike.BIKE_CONNECTION_CLOSED,
                            getDefaultArgument(bikeType));
                    BluetoothStateUtil.setBluetoothStateChangeForBike(null);
                    BluetoothStateUtil.unregisterBluetoothReceiver(activity);
                } catch (Exception e) {
                    e.printStackTrace();
                } finally {
                    result.success("done");

                }
                break;
            case MethodCalls.BluetoothBike.GET_LAST_STATUS:
                BeastBikeStatus bikeStatus = getLastBikeStatus();
                Map data = getBikeData(bikeStatus);
                result.success(data);
                break;
            default:
                result.notImplemented();
        }
    }

    private Map getBikeData(BeastBikeStatus bikeStatus) {
        Map data = new HashMap();
        data.put("lat", bikeStatus.getLat());
        data.put("lng", bikeStatus.getLng());
        data.put("ts", bikeStatus.getTimestamp());
        data.put("gps_fix", bikeStatus.getTimestamp());
        data.put("h_acc", bikeStatus.getHorizontalAccuracyInMeters());
        data.put("speed", bikeStatus.getSpeedInMetersPerSec());
        data.put("odo", bikeStatus.getOdometerInMeter());
        data.put("ignition", bikeStatus.getIgnitionStatus());
        data.put("last_status", bikeStatus.getLastStatusString());
        return data;
    }

    /***
     * This method will return last Bike Status
     */
    private BeastBikeStatus getLastBikeStatus() {
        return BeastRideService.Companion.getLastBikeStatus();

    }

    /***
     * This method will observe the Connected bike states like unlock, lock
     */
    private void ObserveConnectedBikeStates(BikeType bikeType) {
        if (bikeType == BikeType.INVERS){
            InversRideService.Companion.getBikeStatus().observe(lifecycleOwner, new Observer<BikeStatus>() {
                @Override
                public void onChanged(BikeStatus bikeStatus) {
                    Log.d("ObservedBikeStatus", bikeStatus.toString());
                    // Log.d("ble", bikeBLEStatus.toString());
                    if (bikeStatus == bikeBLEStatus)
                        return;
                    bikeBLEStatus = bikeStatus;
                    switch (bikeBLEStatus) {
                        case LOCK:
                            methodChannel.invokeMethod(MethodCalls.BluetoothBike.BIKE_LOCK,
                                    getDefaultArgument(BikeType.INVERS));
                            break;
                        case UNLOCK:
                            methodChannel.invokeMethod(MethodCalls.BluetoothBike.BIKE_UNLOCK,
                                    getDefaultArgument(BikeType.INVERS));
                            break;
                        case ERROR:
                            methodChannel.invokeMethod(MethodCalls.BluetoothBike.BIKE_ERROR,
                                    getDefaultArgument(BikeType.INVERS));
                            break;
                    }
                }
            });
        } else if (bikeType == BikeType.BEAST){

            BeastRideService.Companion.getLockStatus().observe(lifecycleOwner, lockStatus -> {
                showLog("lockStatus", lockStatus.toString());

                switch (lockStatus) {
                    case UNLOCK:
                        methodChannel.invokeMethod(MethodCalls.BluetoothBike.BIKE_UNLOCK,
                                getDefaultArgument(BikeType.BEAST));
                        break;
                    case LOCK_SECURE:
                    case LOCK:
                        methodChannel.invokeMethod(MethodCalls.BluetoothBike.BIKE_LOCK, getDefaultArgument(BikeType.BEAST));
                        break;
                    case BLANK:
                        methodChannel.invokeMethod(MethodCalls.BluetoothBike.BIKE_ERROR,
                                getDefaultArgument(BikeType.BEAST));
                        break;
                    case ERROR:
                        break;
                }
            });
        }
    }

    /***
     * This method will observe the Connection changes of BIKE (Connected,
     * Connecting,Disconnect)
     */
    private void ObserveBikeConnectionStatus(BikeType bikeType) {
        if (bikeType == BikeType.INVERS){
            InversRideService.Companion.getBleStatus().observe(lifecycleOwner, bleStatus -> {
                Log.d("Observed", bleStatus.toString());
                if (bleStatus == BluetoothAdapter.STATE_CONNECTED){
                    methodChannel.invokeMethod(MethodCalls.BluetoothBike.BIKE_CONNECTED,
                            getDefaultArgument(BikeType.INVERS));
                    Log.d("here", bleStatus.toString());
                } else if (bleStatus == BluetoothAdapter.STATE_CONNECTING){
                    methodChannel.invokeMethod(MethodCalls.BluetoothBike.BIKE_CONNECTING,
                            getDefaultArgument(BikeType.INVERS));
                    Log.d("here", bleStatus.toString());
                } else if (bleStatus == BluetoothAdapter.STATE_DISCONNECTED){
                    methodChannel.invokeMethod(MethodCalls.BluetoothBike.BIKE_DISCONNECTED,
                            getDefaultArgument(BikeType.INVERS));
                    Log.d("here", bleStatus.toString());
                } else if (bleStatus == BluetoothAdapter.STATE_DISCONNECTING){
                    methodChannel.invokeMethod(MethodCalls.BluetoothBike.BIKE_DISCONNECTING,
                            getDefaultArgument(BikeType.INVERS));
                    Log.d("here", bleStatus.toString());
                }
            });
        } else if (bikeType == BikeType.BEAST){
            BeastRideService.Companion.getBleStatus().observe(lifecycleOwner, bleStatus -> {
                // CONNECTING, CONNECTED, DISCONNECTING, DISCONNECTED, AUTHENTICATED,
                // AUTHENTICATION_FAILED, UNKNOWN
                showLog("bleStatus", bleStatus.toString());
                switch (bleStatus) {
                    case CONNECTED:
                        methodChannel.invokeMethod(MethodCalls.BluetoothBike.BIKE_CONNECTED,
                                getDefaultArgument(BikeType.BEAST));
                        break;
                    case CONNECTING:
                        methodChannel.invokeMethod(MethodCalls.BluetoothBike.BIKE_CONNECTING,
                                getDefaultArgument(BikeType.BEAST));
                        break;
                    case DISCONNECTED:
                        methodChannel.invokeMethod(MethodCalls.BluetoothBike.BIKE_DISCONNECTED,
                                getDefaultArgument(BikeType.BEAST));
                        break;
                    case DISCONNECTING:
                        methodChannel.invokeMethod(MethodCalls.BluetoothBike.BIKE_DISCONNECTING,
                                getDefaultArgument(BikeType.BEAST));
                        break;
                    case AUTHENTICATED:
                        methodChannel.invokeMethod(MethodCalls.BluetoothBike.AUTHENTICATED,
                                getDefaultArgument(BikeType.BEAST));
                        break;
                    case AUTHENTICATION_FAILED:
                        methodChannel.invokeMethod(MethodCalls.BluetoothBike.AUTHENTICATION_FAILED,
                                getDefaultArgument(BikeType.BEAST));
                        break;
                    case UNKNOWN:
                        break;
                }
            });
        }
    }

    private void ObserveKnobStatus() {
        BeastRideService.Companion.getKnobStatus().observe(lifecycleOwner, knobStatus -> {
            showLog("knobStatus", knobStatus.toString());
            switch (knobStatus) {
                case HANDLE_LOCK:
                    methodChannel.invokeMethod(MethodCalls.BluetoothBike.KnobStatus.HANDLE_LOCK,
                            getDefaultArgument(BikeType.BEAST));
                    break;
                case HANDLE_UNLOCK:
                    methodChannel.invokeMethod(MethodCalls.BluetoothBike.KnobStatus.HANDLE_UNLOCK,
                            getDefaultArgument(BikeType.BEAST));
                    break;
                case IGNITION_ON:
                    methodChannel.invokeMethod(MethodCalls.BluetoothBike.KnobStatus.IGNITION_ON,
                            getDefaultArgument(BikeType.BEAST));
                    break;
                case ERROR:
                    methodChannel.invokeMethod(MethodCalls.BluetoothBike.KnobStatus.ERROR,
                            getDefaultArgument(BikeType.BEAST));
                    break;

            }

        });
    }

    private Map getDefaultArgument(BikeType bikeType) {
        Map map = new HashMap();
        map.put("bikeType", bikeType == BikeType.INVERS ? "invers" : "beast");
        return map;
    }

    private void showLog(String tag, String log) {
        Log.d(tag, log);

    }

    enum BikeType {
        BEAST, INVERS
    }

    @NonNull
    @Override
    public Lifecycle getLifecycle() {
        return super.getLifecycle();
    }

    // @Override
    // protected void onActivityResult(int requestCode, int resultCode, Intent data)
    // {
    // super.onActivityResult(requestCode, resultCode, data);
    // Log.e("BT Plugin result", "" + requestCode);
    // if (requestCode == ActivityRequestCodes.BLUETOOTH_PERMISSION_REQUEST){
    // }
    // }

}
