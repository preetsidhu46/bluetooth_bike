package com.bounceshare.bluetooth_bike.bluetooth;

public interface IBluetoothStateChange {
    void currentBluetoothState(BluetoothState state);
}
