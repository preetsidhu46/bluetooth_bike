package com.bounceshare.bluetooth_bike.bluetooth;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.Log;

public class BluetoothStateUtil {
    public static final int BLUETOOTH_PERMISSION_REQUEST = 1004;

    static boolean isBluetoothDialogShowing = false;

    private static BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

    private static BroadcastReceiver bluetoothStatusReceiver = new BroadcastReceiver() {
        public void onReceive(Context context, Intent intent) {
            String action = intent.getAction();
            assert action != null;
            if (action.equals(BluetoothAdapter.ACTION_STATE_CHANGED)) {
                final int state = intent.getIntExtra(BluetoothAdapter.EXTRA_STATE, BluetoothAdapter.ERROR);
                switch (state) {
                case BluetoothAdapter.STATE_OFF:
                    Log.d("a", "onReceive: STATE OFF");
                    isBluetoothDialogShowing = false;
                    if (iBluetoothStateChange != null)
                        iBluetoothStateChange.currentBluetoothState(BluetoothState.BT_OFF);
                    break;
                case BluetoothAdapter.STATE_TURNING_OFF:
                    // result.success("bluetoothTurnedOff");
                    isBluetoothDialogShowing = false;

                    Log.d("b", "mBroadcastReceiver1: STATE TURNING OFF");
                    break;
                case BluetoothAdapter.STATE_ON:
                    isBluetoothDialogShowing = false;
                    Log.d("c", "mBroadcastReceiver1: STATE ON");
                    if (iBluetoothStateChange != null)
                        iBluetoothStateChange.currentBluetoothState(BluetoothState.BT_ON);
                    if (iBluetoothStateChangeForBike != null)
                        iBluetoothStateChangeForBike.currentBluetoothState(BluetoothState.BT_ON);
                    break;
                case BluetoothAdapter.STATE_TURNING_ON:
                    Log.d("d", "mBroadcastReceiver1: STATE TURNING ON");
                    break;
                }
            }
        }
    };
    private static boolean isRegistered = false;

    private static IBluetoothStateChange iBluetoothStateChange;
    private static IBluetoothStateChange iBluetoothStateChangeForBike;

    public static boolean checkBluetoothPermission(Activity activity) {
        boolean isEnabled = false;
        Log.d("isBtDialogShowing", "" + isBluetoothDialogShowing);
        registerBluetoothReceiver(activity);
        if (mBluetoothAdapter == null) {
            Log.d("tag", "not supported bluetooth");
        } else if (!mBluetoothAdapter.isEnabled()) {
            if (!isBluetoothDialogShowing) {
                isBluetoothDialogShowing = true;
                Intent enableBTIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE);
                activity.startActivityForResult(enableBTIntent, BLUETOOTH_PERMISSION_REQUEST);

            }
        } else {
            isEnabled = true;
            isBluetoothDialogShowing = false;

        }
        return isEnabled;
    }



    public static void setBluetoothStateChange(IBluetoothStateChange listener) {
        iBluetoothStateChange = listener;
    }

    public static void setBluetoothStateChangeForBike(IBluetoothStateChange listener) {
        iBluetoothStateChangeForBike = listener;
    }

    public static boolean isBluetoothEnabled() {
        return mBluetoothAdapter != null && mBluetoothAdapter.isEnabled();
    }

    public static void bleOff() {
        try {
            if (mBluetoothAdapter.isEnabled()) {
                Log.e("ble connection", "off");
                mBluetoothAdapter.disable();
            } else {
                Log.e("ble connection", "on");
            }
        } catch (Exception e) {
            Log.e("error", e.toString());
        }
    }

    private static void registerBluetoothReceiver(Activity activity) {
        try {
            if (!isRegistered) {
                IntentFilter filter = new IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED);
                activity.registerReceiver(bluetoothStatusReceiver, filter);
                isRegistered = true;
            }
        } catch (Exception e) {
            Log.e("error", e.toString());
        }
    }

    public static void unregisterBluetoothReceiver(Activity activity) {
        try {
            isRegistered = false;
            activity.unregisterReceiver(bluetoothStatusReceiver);
        } catch (Exception e) {
            Log.e("error", e.toString());
        }
    }
}
